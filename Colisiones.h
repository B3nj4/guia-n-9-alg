#ifndef COLISIONES_H
#define COLISIONES_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;


// Clase Hash e instanciacion de esta en array
struct set {
	int key;
	int data;
    //int sig;
    //int info;
};

// Clase colisiones
class Colisiones {
    private:
        
    public:
        //Constructor
        Colisiones();

        // OPERACIONES BASICAS PARA LA FUNCION HASH
        int checkPrime(int n);
        int getPrime(int n);
        void init_array(int capacity, set *array);

        // CREACION DEL HASHFUNTION MÁS LA INSERCION DE LOS ELEMENTOS Y SUS KEYS 
        int hashFunction(int key, int capacity);
        void insert(int key, int data, int size, int capacity, set *array, int solu_colisiones);

        // IMPRESION DEL ARREGLO
        void display(int capacity, set *array);

        // SOLUCIONES DE COLISIONES
        void prueba_lineal(set *array, int capacity, int key);
        void prueba_cuadratica(set *array, int capacity, int key);
        void doble_direccion(set *array, int capacity, int key);
        void encadenamiento(set *array, int capacity, int key);

};
#endif