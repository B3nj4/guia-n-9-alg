//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "Colisiones.h"

// MENU DE OPCIONES
void menu(){
    cout << endl;
    cout << "-----------------------------------------" << endl;
    cout << "| 1. Insertar                           |" << endl;
    cout << "| 2. Imprimir                           |" << endl;
    cout << "-----------------------------------------" << endl;
    cout << endl;
}

int main(int argc, char **argv) {
    
    // Variables
    int c, choice, key, data, capacity, size, solu_colisiones;
    
    // Ingresar el tamaño del array
    cout << "Ingrese tamaño: ";
    cin >> capacity;
    
    // INstanciar objetos y variables varias
    size = 0;
    c = 0;
    Colisiones colision;
    set *array;
    
    // INgreso del tipo de solucion de colision (1. Reasignacion - Prueba Lineal, 2.Reasignacion - Prueba cuadratica, 3.Doble direccion Hash, 4.Encadenamiento)
    solu_colisiones = atoi(argv[1]);
 
    if (solu_colisiones <= 0 || solu_colisiones > 4) {
        cout << "Error: Ingrese un dato correcto (numero entre 1 y 4)" << endl;
        return -1;
    }
    
    // Creacion de vector inicializado en 0
    colision.init_array(capacity, array);   

    // Menu
    do{
        menu();
        // Ingresar operacion
        cout << "Ingrese opcion: ";
        cin >> choice;

        switch (choice) {
            // INsertar un elemento con key y data () se meustran los cambios en pantalla
            case 1:
                cout << endl;
                cout << "Ingrese key: ";
                cin >> key;
                cout << "Ingrese dato: ";
                cin >> data;	
            
                colision.insert(key, data, size, capacity, array, solu_colisiones);
                break;

            // Se muestra la lista/ array
            case 2:
                cout << endl;
                colision.display(capacity, array);
                break;                
            
            // INgresar datos nuevamente
            default:
                cout << "Invalid Input" << endl;
        }

        // Para continuar o no las operaciones
        cout << "¿Desea continuar?(1=Si): ";
        cin >> c;

    } while (c == 1);

    return 0;
}
