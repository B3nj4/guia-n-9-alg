prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Hash.cpp Colisiones.cpp
OBJ = Hash.o Colisiones.o
APP = Hash

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)