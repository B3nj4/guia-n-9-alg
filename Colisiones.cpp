#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
using namespace std;

//Librerias
#include "Colisiones.h"

// Constructor
Colisiones::Colisiones() {}

// INICIALIZAR
// Evaluar la prioridad
int Colisiones::checkPrime(int n) {
	int i;

	if (n == 1 || n == 0) {
		return 0;
	}

	for (i = 2; i < n / 2; i++) {
		if (n % i == 0) {
			return 0;
		}
	}
	return 1;
}

// Obtener la prioridad para el Hash funtion
int Colisiones::getPrime(int n) {
	if (n % 2 == 0) {
		n++;
	}

	while (!checkPrime(n)) {
		n += 2;
	}

	return n;
}

void Colisiones::init_array(int capacity, set *array){
	//capacity = getPrime(capacity);
	//array = (struct set *)malloc(capacity * sizeof(struct set));

	for (int i = 0; i < capacity; i++) {
		array[i].key = 0;
		array[i].data = 0;
	}
	
}

// Funcion hash (improtante)
int Colisiones::hashFunction(int key, int capacity) {
	return (key % capacity);
}

// INSERTAR
void Colisiones::insert(int key, int data, int size, int capacity, set *array, int solu_colisiones) {
	int index = hashFunction(key, capacity); // Genera direccion

	if (array[index].data == 0) {  // EN caso de que sea vacio 
		array[index].key = key;
		array[index].data = data;
		size++;
		printf("\n<Key (%d) a sido insertada> \n", key);
		display(capacity, array);
	}
	else if (array[index].key == key) {  // EN caso de que se repita la key (clave)
		array[index].data = data;
	}
	else {
		printf("\n<Ha ocurrido una colision>  \n"); // colisiones
		display(capacity, array); // IMprime para mostrar colision

		// SOlucion de esas colisiones
		// PRUEBA LINEAL
		if(solu_colisiones == 1){
			prueba_lineal(array, capacity, key);
			cout << "Se ha solucionado la colision" << endl;
		} 

		// PRUEBA CUADRATICA
		else if(solu_colisiones == 2){
			prueba_cuadratica(array, capacity, key);
			cout << "Se ha solucionado la colision" << endl;
		}

		// DOBLE DIRECCION
		else if(solu_colisiones == 3){
			doble_direccion(array, capacity, key);
			cout << "Se ha solucionado la colision" << endl;
		}

		// ENCADENAMIENTO
		else if(solu_colisiones == 4){
			encadenamiento(array, capacity, key);
			cout << "Se ha solucionado la colision" << endl;
		}

	}

}


// IMPRESION
void Colisiones::display(int capacity, set *array) {

	// REcorre arreglo para imprimirlo (depende si esta vacio o no) 
	for (int i = 0; i < capacity; i++) {
		if (array[i].data == 0) {
			cout << "\n Array[" << i << "]: (vacio)";
		} 
		else {
			cout << "\n Key: " << array[i].key << " || Array[" << i << "]: " << array[i].data;
		}
	}

	cout << endl << endl;
};


// SOLUCION DE COLISIONES

// Reasignacion - Prueba Lineal
void Colisiones::prueba_lineal(set *array, int capacity, int key){
	int D, DX; // Variables

	D = hashFunction(key, capacity); // Genera direccion

	if((array[D].data != 0) && (array[D].key == key)){
		cout << "La informacion esta en la posicion: " << D << endl;
	} 
	
	else{
		DX = D + 1;

		// Evaluacion	
		while((DX <= capacity) && (array[DX].data != 0) && (array[DX].key != key) && (DX != D)){
			DX = D + 1;
			if(DX == capacity + 1){
				DX = 1;
			}
		}

		// Evaluar la info
		if((array[DX].data == 0) || (DX == D)){
			cout << "La informacion no se encuentra en el arreglo" << endl;
		}

		else{
			cout << "La informacion esta en la posicion: " <<  DX << endl;
		}
	}

}

// Reasignacion - Prueba Cuadratica
void Colisiones::prueba_cuadratica(set *array, int capacity, int key){
	int D, DX, I; // Variables enteras

	D = hashFunction(key, capacity); // Genera direccion

	if((array[D].data != 0) && (array[D].key == key)){
		cout << "La informacion esta en la posicion: " << D << endl;
	} 
	
	else{
		I = 1;
		DX = (D + (I*I)); // Nueva asignacion
		
		// COMparacion
		while((array[DX].data != 0 && array[DX].key != key)){
			I = I + 1;
			DX = (D + (I*I));
			
			if(DX > capacity){
				I = 0;
				DX = 1;
				D = 1;
			}
		}

		// Evaluar la info
		if((array[DX].data == 0)){
			cout << "La informacion no se encuentra en el arreglo" << endl;
		}

		else{
			cout << "La informacion esta en la posicion: " <<  DX << endl;
		}
	}

}

// Reasignacion - Doble direccion
void Colisiones::doble_direccion(set *array, int capacity, int key){
	int D, DX; // Variables enteras

	D = hashFunction(key, capacity); // Genera direccion

	if((array[D].data != 0) && (array[D].key == key)){
		cout << "La informacion esta en la posicion: " << D << endl;
	} 
	
	else{
		DX = hashFunction(D, capacity); // DOble direccion
		
		// COmparacion
		while((DX <= capacity) && (array[DX].data != 0) && (array[DX].key != key) && (DX != D)){	
			DX = hashFunction(DX, capacity);
		}

		// Evaluar la info
		if((array[DX].data == 0) || (array[DX].key != key)){
			cout << "La informacion no se encuentra en el arreglo" << endl;
		}

		else{
			cout << "La informacion esta en la posicion: " <<  DX << endl;
		}
	}
}

// Encadenamiento
void Colisiones::encadenamiento(set *array, int capacity, int key){
	/*int D; // Entero
	set *Q; // TIpo puntero
	Q = array;

	D = hashFunction(key, capacity); // Genera direccion

	if((array[D].data != 0) && (array[D].key == key)){
		cout << "La informacion esta en la posicion: " << D << endl;
	}
	
	else {
		Q = array[D+1]; // apuntador a la lista
		
		while((Q != 0) && (Q.key != key)){
			Q = Q+1;
		}

		// Evaluar la info
		if(Q == 0){
			cout << "La informacion no se encuentra en el arreglo" << endl;
		}

		else {
			cout << "La informacion se encuentra en la lista: " << endl;
		}
	}  */

	/* Hacer D <- H(K) {Genera direcci´on}
		Si ((V[D] <> VACIO) y (V[D] = K) entonces
			Escribir "La informaci´on est´a en la posici´on", D
		Sino
			Hacer Q <- V[D].SIG {apuntador a la lista}
			Mientras ((Q <> VACIO) y Q^.INFO <> K))
				Hacer Q <- Q^.SIG
			Si (Q = VACIO) entonces
				Escribir "La informaci´on no se encuentra en la lista"
			Sino
				Escribir "La informaci´on se encuentra en la lista" 
	*/
	
}
